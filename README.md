# BEP-Cees-Wolfs

All the interesting code is in Main.ipynb the rest are miscallanous notebooks. For Gary specifically: the data is stored on the nas and analysis notebooks can be found under the shared jupyter lab in the CeesW folder.


# Adapting the code
Using the code on a new computer should be as simple as updating the comports and red pitaya names. For implementing new features it is best to just look at how everything is implemented now. Most interesting stuff is in the RPUI class. Its enormous init method loads and links all the widgets. The rest of the methods on that class are simply actions linked to widgets. Threading is used to update plots in the background, of course it should be noted python is not actually multithreaded so sleeping in the update thread is very much essential.
